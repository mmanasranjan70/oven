package com.hcl.oven.util;

public enum OvenFunction {
    FRY(5),
    HEAT(10);
    private int value;

    OvenFunction(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
