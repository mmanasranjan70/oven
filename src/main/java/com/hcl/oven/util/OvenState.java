package com.hcl.oven.util;

public enum OvenState {
    START(5),
    STOP(10);
    private int value;

    OvenState(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
