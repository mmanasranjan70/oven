package com.hcl.oven.repository;

import com.hcl.oven.model.Oven;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OvenRepository extends CrudRepository<Oven, Long> {
}
