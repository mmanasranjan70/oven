package com.hcl.oven.controller;

import com.hcl.oven.service.OvenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OvenController {
    @Autowired
    private OvenService ovenService;

    @GetMapping(value = "/oven")
    public String getOvenDetails(){
        ovenService.getOvenDetails();
        return "Ok";
    }

    @GetMapping(value = "/start_oven/{function}")
    public String startOven(int function){
        ovenService.startOven(function);
        return "started";
    }

    @GetMapping(value = "/start_oven/{time}")
    public String addTime(int addTime){
        ovenService.addTime();
        return "time added";
    }

    @GetMapping(value = "/stop_oven")
    public String stopOven(){
        ovenService.stopOven();
        return "stopped";
    }
}
