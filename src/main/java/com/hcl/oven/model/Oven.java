package com.hcl.oven.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table
public class Oven {
    @Id
    @Column
    private Long id;
    @Column
    private String name;
    @Column
    private Integer timer;
    @Column
    private String startTime;
    @Column
    private String endTime;
    @Column
    private String ovenState;
    @Column
    private String ovenFunction;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTimer() {
        return timer;
    }

    public void setTimer(Integer timer) {
        this.timer = timer;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOvenState() {
        return ovenState;
    }

    public void setOvenState(String ovenState) {
        this.ovenState = ovenState;
    }

    public String getOvenFunction() {
        return ovenFunction;
    }

    public void setOvenFunction(String ovenFunction) {
        this.ovenFunction = ovenFunction;
    }

    @Override
    public String toString() {
        return "Oven{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", timer=" + timer +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", ovenState=" + ovenState +
                ", ovenFunction=" + ovenFunction +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Oven oven = (Oven) o;
        return Objects.equals(id, oven.id) &&
                Objects.equals(name, oven.name) &&
                Objects.equals(timer, oven.timer) &&
                Objects.equals(startTime, oven.startTime) &&
                Objects.equals(endTime, oven.endTime) &&
                ovenState == oven.ovenState &&
                ovenFunction == oven.ovenFunction;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, timer, startTime, endTime, ovenState, ovenFunction);
    }
}
