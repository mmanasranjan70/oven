package com.hcl.oven.service;

import org.springframework.stereotype.Service;

@Service
public interface OvenService {
    void getOvenDetails();

    void startOven();

    void addTime();

    void stopOven();

    void startOven(int function);
}
