package com.hcl.oven.service;

import com.hcl.oven.repository.OvenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OvenServiceImpl implements OvenService {
    @Autowired
    private OvenRepository ovenRepository;

    @Override
    public void getOvenDetails() {

    }

    @Override
    public void startOven() {

    }

    @Override
    public void addTime() {

    }

    @Override
    public void stopOven() {

    }

    @Override
    public void startOven(int function) {

    }
}
